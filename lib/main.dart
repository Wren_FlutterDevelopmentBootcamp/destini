import 'package:destini/story_brain.dart';
import 'story.dart';
import 'package:flutter/material.dart';

void main() => runApp(const Destini());

class Destini extends StatelessWidget {
  const Destini({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Destini',
      theme: ThemeData(
        fontFamily: 'OpenDyslexic',
      ),
      home: const StoryPage(),
    );
  }
}

StoryBrain storyBrain = StoryBrain();
Story currentStory = storyBrain.getNextStory(null);

class StoryPage extends StatefulWidget {
  const StoryPage({Key? key}) : super(key: key);

  @override
  _StoryPageState createState() => _StoryPageState();
}

class _StoryPageState extends State<StoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/background.png'),
            fit: BoxFit.cover,
          ),
        ),
        padding: const EdgeInsets.symmetric(vertical: 50.0, horizontal: 15.0),
        constraints: const BoxConstraints.expand(),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                flex: 12,
                child: Center(
                  child: Text(
                    currentStory.storyTitle,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 25.0,
                      shadows: [
                        Shadow(
                            // topLeft
                            offset: Offset(-1.5, -2.5),
                            color: Colors.black),
                        Shadow(
                            // topRight
                            offset: Offset(2, -2.5),
                            color: Colors.black),
                        Shadow(
                            // bottomRight
                            offset: Offset(1.5, 1.5),
                            color: Colors.black),
                        Shadow(
                            // bottomLeft
                            offset: Offset(-1.5, 1.5),
                            color: Colors.black),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: TextButton(
                  style: TextButton.styleFrom(
                    primary: Colors.white,
                    backgroundColor: Colors.red,
                    shape: const RoundedRectangleBorder(),
                  ),
                  onPressed: () {
                    //Choice 1 made by user.
                    progressStory(1);
                  },
                  child: Text(
                    currentStory.choice1,
                    style: const TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              Expanded(
                flex: 2,
                child: TextButton(
                  style: TextButton.styleFrom(
                    primary: Colors.white,
                    backgroundColor: Colors.blue,
                    shape: const RoundedRectangleBorder(),
                  ),
                  onPressed: () {
                    //Choice 2 made by user.
                    progressStory(2);
                  },
                  // color: Colors.blue,
                  child: Text(
                    currentStory.choice2,
                    style: const TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void progressStory(int choice) {
    setState(() {
      currentStory = storyBrain.getNextStory(choice);
    });
  }
}
